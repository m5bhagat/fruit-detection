# Fruit Detection

Fruit Detection is an image classifer built using Tensorflow for recognizing pictures of fruits

## Usage

Clone the project and install dependencies:

```bash
git clone https://git.uwaterloo.ca/m5bhagat/fruit-detection
pipenv install
```
To make a prediction using the model, find a 100 x 100 pixel image, and use its path as an argument to the predict.py module. There exists a set of sample images in the sample_images directory.

Example:
```bash
python ./predict.py sample_images/Tomato/144_100.jpg
```

## Model Architechture
This model uses a convolutional neural network. The model architechture, parameters and weights are available in Tensorflow Saved Model format

Saved Model location:
```bash
cd saved_model
```
