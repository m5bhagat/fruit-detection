import os
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.image import ImageDataGenerator, load_img, img_to_array
import numpy as np
import sys

saved_model_path = os.path.join(os.getcwd(), 'saved_model/')
path_to_image = sys.argv[1]
class_names = ['Apple Braeburn', 'Apricot', 'Avocado', 'Banana', 'Banana Red', 'Beetroot', 'Blueberry', 'Cactus fruit', 'Cantaloupe', 'Cauliflower', 'Cherry', 'Chestnut', 'Clementine', 'Cocos', 'Dates', 'Eggplant', 'Ginger Root', 'Granadilla', 'Grape Pink', 'Grape White', 'Guava', 'Hazelnut', 'Kiwi', 'Kumquats', 'Lemon', 'Limes', 'Lychee', 'Mandarine', 'Mango', 'Onion Red', 'Onion Red Peeled', 'Onion White', 'Orange', 'Papaya', 'Passion Fruit', 'Peach', 'Pear', 'Pepper Green', 'Pepper Red', 'Pepper Yellow', 'Pineapple', 'Plum', 'Pomegranate', 'Potato', 'Potato Sweet', 'Raspberry', 'Strawberry', 'Tomato', 'Walnut'] 

def load_img_as_array(path_to_img):
    if(path_to_img == None):
        return None
    img = load_img(path_to_img)
    return img_to_array(img)

def load_model(saved_model_path):
    return tf.keras.models.load_model(saved_model_path)

def predict(model, imgArray):
    imgArray = np.array([imgArray])
    predictions = model.predict(imgArray)
    prediction = np.argmax(predictions[0])
    if(prediction > 0 and prediction < len(class_names)):
        return class_names[prediction]

model = load_model(saved_model_path)
imgArray = load_img_as_array(path_to_image)
if(model != None):
    print("This is a " + str(predict(model, imgArray)))